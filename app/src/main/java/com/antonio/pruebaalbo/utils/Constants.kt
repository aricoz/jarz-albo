package com.antonio.pruebaalbo.utils

class Constants {
    companion object {
        val BASE_URL = "https://api.punkapi.com/"
        val PER_PAGE = 20
    }
}