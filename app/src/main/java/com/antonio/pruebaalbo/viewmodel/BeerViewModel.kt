package com.chethan.demoproject

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.antonio.pruebaalbo.model.Beer
import com.antonio.pruebaalbo.repository.DataRepository
import org.koin.standalone.KoinComponent


class BeerViewModel(val dataRepository: DataRepository) : ViewModel(), KoinComponent {

    var listOfBeers = MutableLiveData<List<Beer>>()

    init {
        listOfBeers.value = listOf()
    }

    fun getProducts(page: Int, perPage: Int) {
        dataRepository.getProducts(page, perPage, object : DataRepository.OnProductData {
            override fun onSuccess(data: List<Beer>) {
                listOfBeers.value = data
            }

            override fun onFailure(message: String) {
                //REQUEST FAILED
            }
        })
    }
}