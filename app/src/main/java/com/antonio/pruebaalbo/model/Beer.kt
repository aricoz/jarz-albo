package com.antonio.pruebaalbo.model

import java.io.Serializable

data class Beer(
    var id: Int,
    var name: String,
    var image_url: String,
    var tagline: String,
    var description: String,
    var first_brewed: String,
    var food_pairing: List<String>
) : Serializable