package com.antonio.pruebaalbo.repository

import com.antonio.pruebaalbo.NetWorkApi
import com.antonio.pruebaalbo.model.Beer
import retrofit2.Call
import retrofit2.Response

class DataRepository(val netWorkApi: NetWorkApi) {
    fun getProducts(page: Int, perPage: Int, onProductData: OnProductData) {
        netWorkApi.getProducts(page, perPage).enqueue(object : retrofit2.Callback<List<Beer>> {
            override fun onResponse(call: Call<List<Beer>>, response: Response<List<Beer>>) {
                if (response != null && response.body() != null && response.code() == 200) {
                    onProductData.onSuccess((response.body() as List<Beer>))
                } else {
                    onProductData.onFailure("Error")
                }
            }

            override fun onFailure(call: Call<List<Beer>>, t: Throwable) {
                onProductData.onFailure(t.toString())
            }
        })
    }

    interface OnProductData {
        fun onSuccess(data: List<Beer>)
        fun onFailure(message: String)
    }
}

