package com.antonio.pruebaalbo


import com.antonio.pruebaalbo.model.Beer
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NetWorkApi{

    @GET("/v2/beers")
    fun getProducts(@Query("page") page: Int, @Query("per_page") perPage: Int): Call<List<Beer>>

}