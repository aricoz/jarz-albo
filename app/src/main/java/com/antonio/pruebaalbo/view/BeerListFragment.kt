package com.antonio.pruebaalbo.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.antonio.pruebaalbo.R
import com.antonio.pruebaalbo.model.Beer
import com.antonio.pruebaalbo.utils.Constants
import com.antonio.pruebaalbo.utils.EndlessRecyclerViewScrollListener
import com.chethan.demoproject.BeerViewModel
import kotlinx.android.synthetic.main.fragment_beer_list.*
import org.koin.android.viewmodel.ext.android.viewModel


class BeerListFragment : Fragment() {
    private val beerListModel: BeerViewModel by viewModel()
    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    private lateinit var owner: LifecycleOwner
    private var beerList: MutableList<Beer> = ArrayList()
    private lateinit var productListAdapter: BeerListAdapter
    private var pageToSend = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_beer_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        owner = this

        val recyclerView = view?.findViewById<RecyclerView>(R.id.recyclerView)
        val linearLM = LinearLayoutManager(view!!.context, RecyclerView.VERTICAL, false)
        recyclerView!!.layoutManager = linearLM

        scrollListener = object : EndlessRecyclerViewScrollListener(linearLM) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                loadData(page)
            }
        }
        recyclerView.addOnScrollListener(scrollListener as EndlessRecyclerViewScrollListener)

        swiperefresh.setOnRefreshListener {
            loadData(1)
        }

        swiperefresh.isRefreshing = true
        loadData(1)
    }

    override fun onResume() {
        super.onResume()
        (activity as ScrollingActivity).changeToolbarTitle(getString(R.string.app_name))
        (activity as ScrollingActivity).backButton(false)
    }

    fun loadData(page: Int) {
        pageToSend = page
        beerListModel.getProducts(pageToSend, Constants.PER_PAGE)
        if (!beerListModel.listOfBeers.hasObservers()) {
            beerListModel.listOfBeers.observe(owner, Observer(function = fun(beers: List<Beer>?) {
                if (pageToSend == 1) {
                    scrollListener?.resetState()
                    beerList.clear()
                    beerList.addAll(beers!!)
                    productListAdapter = BeerListAdapter(this.beerList)
                    recyclerView.adapter = productListAdapter
                    productListAdapter.setItemClickListener(object : BeerListAdapter.ItemClickListener {
                        override fun onItemClick(view: View, position: Int) {
                            val newFragment = BeerDetailFragment.newInstance(beerList.get(position))
                            val transaction = fragmentManager!!.beginTransaction()
                            transaction.replace(R.id.frag_container, newFragment)
                            transaction.addToBackStack(null)
                            transaction.commit()
                        }
                    })
                } else {
                    beerList.addAll(beers!!)
                    productListAdapter.notifyDataSetChanged()
                }

                swiperefresh.isRefreshing = false
            }))
        }
    }
}
