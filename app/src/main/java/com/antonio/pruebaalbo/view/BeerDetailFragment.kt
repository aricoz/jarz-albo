package com.antonio.pruebaalbo.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.antonio.pruebaalbo.R
import com.antonio.pruebaalbo.model.Beer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_beer.*


class BeerDetailFragment : Fragment() {
    private lateinit var beer: Beer

    companion object {
        const val KEY_PRODUCT = "KEY_PRODUCT"

        fun newInstance(beer: Beer): BeerDetailFragment {
            val args = Bundle()
            args.putSerializable(KEY_PRODUCT, beer)
            val fragment = BeerDetailFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { beer = it.getSerializable(KEY_PRODUCT) as Beer }
    }

    override fun onResume() {
        super.onResume()
        (activity as ScrollingActivity).changeToolbarTitle(beer.name)
        (activity as ScrollingActivity).backButton(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_beer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val options: RequestOptions = RequestOptions()
            .fitCenter()
            .placeholder(R.mipmap.ic_launcher_round)
            .error(R.mipmap.ic_launcher_round)

        Glide.with(activity)
            .load(beer.image_url)
            .apply(options).into(imageBeer)

        taglineBeer.text = beer.tagline
        descriptionBeer.text = beer.description
        firstBeer.text = beer.first_brewed

        foodBeer.setLayoutManager(LinearLayoutManager(activity))
        val adapter = SimpleTextAdapter(beer.food_pairing)
        foodBeer.setAdapter(adapter)
    }
}