package com.antonio.pruebaalbo.view

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.antonio.pruebaalbo.R
import kotlinx.android.synthetic.main.activity_scrolling.*


class ScrollingActivity : AppCompatActivity() {
    private lateinit var act: Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)

        act = this
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportFragmentManager.beginTransaction()
            .replace(R.id.frag_container, BeerListFragment()).commit()
    }

    fun changeToolbarTitle(title: String) {
        toolbar.title = title
    }

    fun backButton(addBack: Boolean) {
        if (addBack) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
            toolbar.setNavigationOnClickListener { act.onBackPressed() }
        } else  {
            toolbar.setNavigationIcon(null)
            toolbar.setNavigationOnClickListener { }
        }
    }
}
