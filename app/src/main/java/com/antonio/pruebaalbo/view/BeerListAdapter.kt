package com.antonio.pruebaalbo.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.antonio.pruebaalbo.R
import com.antonio.pruebaalbo.model.Beer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class BeerListAdapter(private val beerList: List<Beer>) :
    RecyclerView.Adapter<BeerListAdapter.ViewHolder>() {
    private var onItemClickListener: ItemClickListener? = null
    private lateinit var ctx: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0?.context).inflate(R.layout.adapter_beer_list, p0, false)
        ctx = p0?.context
        return ViewHolder(v);
    }

    override fun getItemCount(): Int {
        return beerList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.name?.text = beerList[position].name
        viewHolder.tagline?.text = beerList[position].tagline
        viewHolder.itemView.setOnClickListener {
            onItemClickListener?.onItemClick(viewHolder.itemView, position)
        }

        val options: RequestOptions = RequestOptions()
            .fitCenter()
            .placeholder(R.mipmap.ic_launcher_round)
            .error(R.mipmap.ic_launcher_round)

        Glide.with(ctx)
            .load(beerList[position].image_url)
            .apply(options).into(viewHolder.image)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.nameBeer)
        val image = itemView.findViewById<ImageView>(R.id.imageBeer)
        val tagline = itemView.findViewById<TextView>(R.id.taglineBeer)
    }


    fun setItemClickListener(clickListener: ItemClickListener) {
        onItemClickListener = clickListener
    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}